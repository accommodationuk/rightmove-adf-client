<?php
/**
 * This file contains the RightmoveADF class.
 *
 * @package Accommodationuk\RightmoveADF
 * @author  Jacob Wyke <jacob@frozensheep.com>
 * @license MIT
 *
 */

namespace Accommodationuk\RightmoveADF;

use Accommodationuk\RightmoveADF\Exception\UnknownRequestTypeException;
use Accommodationuk\RightmoveADF\Request\AddFeaturedProperty;
use Accommodationuk\RightmoveADF\Request\AddPremiumListing;
use Accommodationuk\RightmoveADF\Request\GetBranchEmails;
use Accommodationuk\RightmoveADF\Request\GetBranchPerformance;
use Accommodationuk\RightmoveADF\Request\GetBranchPhoneLeads;
use Accommodationuk\RightmoveADF\Request\GetBranchPropertyList;
use Accommodationuk\RightmoveADF\Request\GetBrandEmails;
use Accommodationuk\RightmoveADF\Request\GetBrandPhoneLeads;
use Accommodationuk\RightmoveADF\Request\GetPropertyEmails;
use Accommodationuk\RightmoveADF\Request\GetPropertyPerformance;
use Accommodationuk\RightmoveADF\Request\RemoveFeaturedProperty;
use Accommodationuk\RightmoveADF\Request\RemoveProperty;
use Accommodationuk\RightmoveADF\Request\SendProperty;
use Frozensheep\Synthesize\Synthesizer;

/**
 * RightmoveADF Class
 *
 * Class for the RightmoveADF.
 *
 * @package Accommodationuk\RightmoveADF
 *
 */
class RightmoveADF
{
    use Synthesizer;

    const TEST = 0;
    const LIVE = 1;
    const DEBUG = 1;

    const SendProperty = 1;
    const RemoveProperty = 2;
    const GetBranchPropertyList = 3;
    const AddPremiumListing = 4;
    const AddFeaturedProperty = 5;
    const RemoveFeaturedProperty = 6;
    const GetPropertyPerformance = 7;
    const GetBranchPerformance = 8;
    const GetBrandEmails = 9;
    const GetBranchEmails = 10;
    const GetBrandPhoneLeads = 11;
    const GetBranchPhoneLeads = 12;
    const GetPropertyEmails = 13;

    /**
     * @var array $arrSynthesize The synthesize array.
     */
    protected $arrSynthesize = array(
        'cert_file' => array('type' => 'string'),
        'cert_pass' => array('type' => 'string'),
        'environment' => array('type' => 'int', 'min' => 0, 'max' => 1, 'default' => RightmoveADF::TEST)
    );

    /**
     * Class Constructor
     *
     * @param string $strCertFile The certificate file.
     * @param string $strCertPass The certificate password.
     * @param int $numEnvironment The environment to run in.
     *
     * @return void
     */
    public function __construct(string $strCertFile, string $strCertPass, int $numEnvironment = RightmoveADF::TEST)
    {
        $this->cert_file = $strCertFile;
        $this->cert_pass = $strCertPass;
        $this->environment = $numEnvironment;
    }

    /**
     * Create Request Method
     *
     * Returns the request object for the given request type.
     * @param int $numRequestType The request type.
     * @return object
     */
    public static function createRequest(int $numRequestType)
    {
        switch ($numRequestType) {
            case RightmoveADF::SendProperty:
                return new SendProperty();
            case RightmoveADF::RemoveProperty:
                return new RemoveProperty();
            case RightmoveADF::GetBranchPropertyList:
                return new GetBranchPropertyList();
            case RightmoveADF::AddPremiumListing:
                return new AddPremiumListing();
            case RightmoveADF::AddFeaturedProperty:
                return new AddFeaturedProperty();
            case RightmoveADF::RemoveFeaturedProperty:
                return new RemoveFeaturedProperty();
            case RightmoveADF::GetPropertyPerformance:
                return new GetPropertyPerformance();
            case RightmoveADF::GetBranchPerformance:
                return new GetBranchPerformance();
            case RightmoveADF::GetBrandEmails:
                return new GetBrandEmails();
            case RightmoveADF::GetBranchEmails:
                return new GetBranchEmails();
            case RightmoveADF::GetBrandPhoneLeads:
                return new GetBrandPhoneLeads();
            case RightmoveADF::GetBranchPhoneLeads:
                return new GetBranchPhoneLeads();
            case RightmoveADF::GetPropertyEmails:
                return new GetPropertyEmails();
            default:
                throw new UnknownRequestTypeException();
        }
    }

    /**
     * Send Method
     *
     * Returns the request object for the given request type.
     * @param $objRequest
     * @param bool $boolDebug
     *
     * @return mixed
     */
    public function send($objRequest, bool $boolDebug = false)
    {
        $strURL = $objRequest->getURL($this->environment);

        return Curl::send(json_encode($objRequest), $strURL, $this->cert_file, $this->cert_pass, $boolDebug);
    }
}
