<?php
/**
*	This file contains the Featured Property Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;
use Accommodationuk\RightmoveADF\Values\FeaturedPropertyTypes;

/**
*	Featured Property Group Class
*
*	Class to handle Featured Property group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class FeaturedProperty implements GroupInterface, \JsonSerializable
{
    use Synthesizer;

    protected $arrSynthesize = array(
        'featured_property_type' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\FeaturedPropertyTypes', 'required' => true)
    );
}
