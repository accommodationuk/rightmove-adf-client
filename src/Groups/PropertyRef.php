<?php
/**
*	This file contains the Property Ref Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;

/**
*	Property Ref Group Class
*
*	Class to handle Property Ref group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class PropertyRef implements GroupInterface, \JsonSerializable
{
    use Synthesizer;

    protected $arrSynthesize = array(
        'agent_ref' => array('type' => 'string', 'required' => true, 'max' => 80)
    );
}
