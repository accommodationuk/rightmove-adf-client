<?php
/**
*	This file contains the Property Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;
use Accommodationuk\RightmoveADF\Groups\Address;
use Accommodationuk\RightmoveADF\Groups\Price;
use Accommodationuk\RightmoveADF\Groups\Details;
use Accommodationuk\RightmoveADF\Groups\Principal;
use Accommodationuk\RightmoveADF\Values\PropertyTypes;
use Accommodationuk\RightmoveADF\Values\Statuses;
use Accommodationuk\RightmoveADF\Values\LetTypes;

/**
*	Property Group Class
*
*	Class to handle Property group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class Property implements GroupInterface, \JsonSerializable
{
    use Synthesizer;

    protected $arrSynthesize = array(
        'agent_ref' => array('type' => 'string', 'required' => true, 'max' => 80),
        'published' => array('type' => 'boolean', 'required' => true),
        'property_type' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\PropertyTypes', 'required' => true),
        'status' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\Statuses', 'required' => true),
        'new_home' => array('type' => 'boolean'),
        'student_property' => array('type' => 'boolean'),
        'house_flat_share' => array('type' => 'boolean'),
        'create_date' => array('type' => 'datetime', 'format' => 'd-m-Y H:i:s', 'autoinit' => false),
        'update_date' => array('type' => 'datetime', 'format' => 'd-m-Y H:i:s', 'autoinit' => false),
        'date_available' => array('type' => 'datetime', 'format' => 'd-m-Y', 'autoinit' => false),
        'contract_months' => array('type' => 'int'),
        'minimum_term' => array('type' => 'int'),
        'let_type' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\LetTypes'),
        'address' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Address', 'required' => true),
        'price_information' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Price', 'required' => true),
        'details' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Details', 'required' => true),
        'media' => array('type' => 'objectarray', 'class' => 'Accommodationuk\RightmoveADF\Groups\Media', 'max' => 999),
        'principal' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Principal')
    );
}
