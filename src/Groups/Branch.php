<?php
/**
*	This file contains the Branch Group model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Groups;

use Accommodationuk\RightmoveADF\Groups\GroupInterface;
use Frozensheep\Synthesize\Synthesizer;
use Accommodationuk\RightmoveADF\Values\Channels;

/**
*	Branch Group Class
*
*	Class to handle Branch group.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class Branch implements GroupInterface, \JsonSerializable
{
    use Synthesizer;

    protected $arrSynthesize = array(
        'branch_id' => array('type' => 'int', 'required' => true),
        'channel' => array('type' => 'enum', 'class' => 'Accommodationuk\RightmoveADF\Values\Channels', 'required' => true),
        'overseas' => array('type' => 'boolean')
    );
}
