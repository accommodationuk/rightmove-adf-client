<?php
/**
 *  This file contains the Send Property Request class.
 *
 * @package  Accommodationuk\RightmoveADF
 * @author    Jacob Wyke <jacob@frozensheep.com>
 * @license  MIT
 *
 */

namespace Accommodationuk\RightmoveADF\BoominRTDF\Request;

/**
 *  Send Property Class
 *
 *  Class for the send property request.
 *
 * @package  Accommodationuk\BoominRTDF
 *
 */
class SendProperty extends \Accommodationuk\RightmoveADF\Request\SendProperty
{
    /**
     * @var string $_strLiveURL The live request URL.
     */
    protected string $_strLiveURL = 'https://rtdf.boomin.com/v1/property/sendpropertydetails';

    /**
     * @var string $_strTestURL The test request URL.
     */
    protected string $_strTestURL = 'https://training-rtdf.boomin.dev/v1/property/sendpropertydetails';

    /**
     * @var array $arrSynthesize The synthesize array.
     */
    protected $arrSynthesize = array(
        'network' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Network', 'required' => true),
        'branch' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Branch', 'required' => true),
        'property' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Property', 'required' => true)
    );
}
