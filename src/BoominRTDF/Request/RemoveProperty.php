<?php
/**
 *  This file contains the Remove Property Request class.
 *
 * @package  Accommodationuk\RightmoveADF
 * @author    Jacob Wyke <jacob@frozensheep.com>
 * @license  MIT
 *
 */

namespace Accommodationuk\RightmoveADF\BoominRTDF\Request;

/**
 *  Remove Property Class
 *
 *  Class for remove property request.
 *
 * @package  Accommodationuk\BoominRTDF
 *
 */
class RemoveProperty extends \Accommodationuk\RightmoveADF\Request\RemoveProperty
{
    /**
     * @var string $_strLiveURL The live request URL.
     */
    protected string $_strLiveURL = 'https://rtdf.boomin.com/v1/property/removeproperty';

    /**
     * @var string $_strTestURL The test request URL.
     */
    protected string $_strTestURL = 'https://training-rtdf.boomin.dev/v1/property/removeproperty';
}
