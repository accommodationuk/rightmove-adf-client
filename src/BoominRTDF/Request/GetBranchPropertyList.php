<?php
/**
 *  This file contains the Get Branch Property List Request model class.
 *
 * @package  Accommodationuk\RightmoveADF
 * @author    Jacob Wyke <jacob@frozensheep.com>
 * @license  MIT
 *
 */
namespace Accommodationuk\RightmoveADF\BoominRTDF\Request;

/**
 *  Get Branch Property List Class
 *
 *  Class for get branch property list request.
 *
 * @package  Accommodationuk\BoominRTDF
 *
 */
class GetBranchPropertyList extends \Accommodationuk\RightmoveADF\Request\GetBranchPropertyList
{
    /**
     * @var string $_strLiveURL The live request URL.
     */
    protected string $_strLiveURL = 'https://rtdf.boomin.com/v1/property/getbranchpropertylist';

    /**
     * @var string $_strTestURL The test request URL.
     */
    protected string $_strTestURL = 'https://training-rtdf.boomin.dev/v1/property/getbranchpropertylist';
}
