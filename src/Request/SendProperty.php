<?php
/**
*	This file contains the Send Property Request class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Request;

use Accommodationuk\RightmoveADF\Request\RequestBase;
use Accommodationuk\RightmoveADF\Groups\Network;
use Accommodationuk\RightmoveADF\Groups\Branch;
use Accommodationuk\RightmoveADF\Groups\Property;

/**
*	Send Property Class
*
*	Class for the send property request.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class SendProperty extends RequestBase
{

    /**
    *	@var string $_strLiveURL The live request URL.
    */
    protected string $_strLiveURL = 'https://adfapi.rightmove.co.uk/v1/property/sendpropertydetails';

    /**
    *	@var string $_strTestURL The test request URL.
    */
    protected string $_strTestURL = 'https://adfapi.adftest.rightmove.com/v1/property/sendpropertydetails';

    /**
    *	@var array $arrSynthesize The synthesize array.
    */
    protected $arrSynthesize = array(
        'network' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Network', 'required' => true),
        'branch' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Branch', 'required' => true),
        'property' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Property', 'required' => true)
    );
}
