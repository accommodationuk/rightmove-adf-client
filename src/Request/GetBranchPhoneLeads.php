<?php
/**
*	This file contains the Get Branch Phone Leads Request model class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Request;

use Accommodationuk\RightmoveADF\Request\RequestBase;
use Accommodationuk\RightmoveADF\Groups\Network;
use Accommodationuk\RightmoveADF\Groups\Branch;
use Accommodationuk\RightmoveADF\Groups\ExportPeriodTime;

/**
*	Get Branch Phone Leads Class
*
*	Class for the get branch phone leads request.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class GetBranchPhoneLeads extends RequestBase
{

    /**
    *	@var string $_strLiveURL The live request URL.
    */
    protected string $_strLiveURL = 'https://adfapi.rightmove.co.uk/v1/property/getbranchphoneleads';

    /**
    *	@var string $_strTestURL The test request URL.
    */
    protected string $_strTestURL = 'https://adfapi.adftest.rightmove.com/v1/property/getbranchphoneleads';

    /**
    *	@var array $arrSynthesize The synthesize array.
    */
    protected $arrSynthesize = array(
        'network' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Network', 'required' => true),
        'branch' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\Branch', 'required' => true),
        'export_period' => array('type' => 'object', 'class' => 'Accommodationuk\RightmoveADF\Groups\ExportPeriodTime', 'required' => true)
    );
}
