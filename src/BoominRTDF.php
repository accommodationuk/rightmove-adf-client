<?php

namespace Accommodationuk\RightmoveADF;

use Accommodationuk\RightmoveADF\Exception\UnknownRequestTypeException;
use Accommodationuk\RightmoveADF\BoominRTDF\Request\GetBranchPropertyList;
use Accommodationuk\RightmoveADF\BoominRTDF\Request\RemoveProperty;
use Accommodationuk\RightmoveADF\BoominRTDF\Request\SendProperty;

use Frozensheep\Synthesize\Synthesizer;

/**
 * BoominRTDF Class
 *
 * BoominRTDF supports an almost 1:1 map with Rightmove RTDF so we override the original class.
 *
 * @package Accommodationuk\BoominRTDF
 */
class BoominRTDF
{
    use Synthesizer;

    const TEST = 0;
    const LIVE = 1;
    const DEBUG = 1;

    const SendProperty = 1;
    const RemoveProperty = 2;
    const GetBranchPropertyList = 3;

    /**
     * @var array $arrSynthesize The synthesize array.
     */
    protected $arrSynthesize = array(
        'cert_file' => array('type' => 'string'),
        'cert_pass' => array('type' => 'string'),
        'environment' => array('type' => 'int', 'min' => 0, 'max' => 1, 'default' => BoominRTDF::TEST)
    );

    /**
     * Class Constructor
     *
     * @param string $strCertFile The certificate file.
     * @param string $strCertPass The certificate password.
     * @param int $numEnvironment The environment to run in.
     *
     * @return void
     */
    public function __construct(string $strCertFile, string $strCertPass = '', int $numEnvironment = BoominRTDF::TEST)
    {
        $this->cert_file = $strCertFile;
        $this->cert_pass = $strCertPass;
        $this->environment = $numEnvironment;
    }

    /**
     * Create Request Method
     *
     * Returns the request object for the given request type.
     * @param int $numRequestType The request type.
     * @return object
     */
    public static function createRequest(int $numRequestType)
    {
        switch ($numRequestType) {
            case BoominRTDF::SendProperty:
                return new SendProperty();
            case BoominRTDF::RemoveProperty:
                return new RemoveProperty();
            case BoominRTDF::GetBranchPropertyList:
                return new GetBranchPropertyList();
            default:
                throw new UnknownRequestTypeException();
        }
    }

    /**
     * Send Method
     *
     * Returns the request object for the given request type.
     * @param $objRequest
     * @param bool $boolDebug
     *
     * @return mixed
     */
    public function send($objRequest, bool $boolDebug = false)
    {
        $strURL = $objRequest->getURL($this->environment);

        return Curl::send(json_encode($objRequest), $strURL, $this->cert_file, $this->cert_pass, $boolDebug);
    }
}
