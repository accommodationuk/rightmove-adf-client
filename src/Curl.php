<?php
/**
*	This file contains the Curl class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF;

/**
 * Class Curl
 * @package Accommodationuk\RightmoveADF
 */
class Curl
{
    /**
     * @param $strJSON
     * @param $strURL
     * @param $strCertFile
     * @param $strCertPass
     * @param bool $boolDebug
     *
     * @return mixed
     */
    public static function send($strJSON, $strURL, $strCertFile, $strCertPass, bool $boolDebug)
    {
        $resCurl = curl_init();

        curl_setopt($resCurl, CURLOPT_URL, $strURL);
        curl_setopt($resCurl, CURLOPT_CERTINFO, 1);
        curl_setopt($resCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($resCurl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($resCurl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($resCurl, CURLOPT_FAILONERROR, 0);
        curl_setopt($resCurl, CURLOPT_SSLCERT, $strCertFile);
        curl_setopt($resCurl, CURLOPT_SSLCERTTYPE, 'PEM');

        if ($strCertPass) {
            curl_setopt($resCurl, CURLOPT_SSLCERTPASSWD, $strCertPass);
        }

        curl_setopt($resCurl, CURLOPT_POST, 1);
        curl_setopt($resCurl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($resCurl, CURLOPT_POSTFIELDS, $strJSON);

        if ($boolDebug) {
            curl_setopt($resCurl, CURLOPT_VERBOSE, true);
        }

        $strResponse = curl_exec($resCurl);

        return json_decode($strResponse);
    }
}
