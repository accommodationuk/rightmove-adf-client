<?php
/**
*	This file contains the Channels Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Channels Enum Class
*
*	Class for the different channel options.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class Channels extends ValuesBase
{
    const Sales = 1;
    const Lettings = 2;
}
