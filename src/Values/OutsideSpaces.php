<?php
/**
*	This file contains the Outside Spaces Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Outside Spaces Enum Class
*
*	Class for the different outside spaces.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class OutsideSpaces extends ValuesBase
{
    const BackGarden = 29;
    const CommunalGarden = 30;
    const EnclosedGarden = 31;
    const FrontGarden = 32;
    const PrivateGarden = 33;
    const RearGarden = 34;
    const Terrace = 35;
    const Patio = 36;
}
