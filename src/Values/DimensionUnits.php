<?php
/**
*	This file contains the Dimension Units Enum class.
*
*	@package	Accommodationuk\RightmoveADF
*	@author		Jacob Wyke <jacob@frozensheep.com>
*	@license	MIT
*
*/

namespace Accommodationuk\RightmoveADF\Values;

use Accommodationuk\RightmoveADF\Values\ValuesBase;

/**
*	Dimension Units Enum Class
*
*	Class for the different dimension units.
*
*	@package	Accommodationuk\RightmoveADF
*
*/
class DimensionUnits extends ValuesBase
{
    const Metres =  5;
    const Centimetres = 6;
    const Millimetres = 7;
    const Feet = 8;
    const Inches = 9;
}
